{-# LANGUAGE BangPatterns, OverloadedStrings, ScopedTypeVariables, TemplateHaskell #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Web.Connect.QueryStringHashTests
  (
    main
  , defaultTestGroup
) where

import           Control.Applicative
import           Test.Tasty
import           Test.Tasty.TH
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck
import qualified Test.QuickCheck as QC
import qualified Data.Map              as Map
import qualified Data.Text             as T
import qualified Data.Text.Lazy        as TL
import           Data.Maybe
import           Data.String (fromString, IsString)
import           Network.URI
import           Web.Connect.QueryStringHash

defaultTestGroup :: TestTree
defaultTestGroup = $(testGroupGenerator)

main :: IO ()
main = defaultMain defaultTestGroup

-- See: https://developer.atlassian.com/static/connect/docs/concepts/authentication.html#qsh

uri :: String -> URI
uri input = fromMaybe nullURI $ parseURI input

case_toCanonicalUrl= do
    let baseUrl = uri "http://localhost:2990"
        input = "http://localhost:2990/path/to/service?zee_last=param&repeated=parameter+1&first=param&repeated=parameter+2"
        canonical = "GET&/path/to/service&first=param&repeated=parameter%201,parameter%202&zee_last=param"
    Just canonical @=? toCanonicalUrl GET baseUrl input

case_toCanonicalUrlStripContextPath = do
    let baseUrl = uri "http://localhost:2990/jira"
        input = "http://localhost:2990/jira/path/to/service?zee_last=param&repeated=parameter+1&first=param&repeated=parameter+2"
        canonical = "GET&/path/to/service&first=param&repeated=parameter%201,parameter%202&zee_last=param"
    Just canonical @=? toCanonicalUrl GET baseUrl input

case_toCanonicalPercentEncodeParamKeysAndValues = do
    let baseUrl = uri "http://localhost:2990"
        input = "http://localhost:2990/path/to/service?a=p*+1"
        canonical = "GET&/path/to/service&a=p%2A%201"
    Just canonical @=? toCanonicalUrl GET baseUrl input

case_toCanonicalUrlQueryParamSort = do
    let baseUrl = uri "http://localhost:2990"
        input = "http://localhost:2990/path/to/service?a=1&A=2&b=3&B=4"
        canonical = "GET&/path/to/service&A=2&B=4&a=1&b=3"
    Just canonical @=? toCanonicalUrl GET baseUrl input

case_toCanonicalIgnoreJwtParam = do
    let baseUrl = uri "http://localhost:2990"
        input = "http://localhost:2990/path/to/service?a=1&jwt=foo"
        canonical = "GET&/path/to/service&a=1"
    Just canonical @=? toCanonicalUrl GET baseUrl input

case_createQueryStringHash = do
    let baseUrl = uri "http://localhost:2990"
        input = "http://localhost:2990/path/to/service?a=1&A=2&b=3&B=4"
        canonical = "70282c7cf82834bd5a3d6dacda1b4ccd5cf5860a63a1fa2fb86b64d576e6a1d5"
    Just canonical @=? createQueryStringHash GET baseUrl input


