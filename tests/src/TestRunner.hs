module Main where

import qualified Web.Connect.QueryStringHashTests
import           Test.Tasty

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [
                    Web.Connect.QueryStringHashTests.defaultTestGroup
                ]

