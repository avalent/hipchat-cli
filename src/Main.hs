module Main (main) where

-- HipChat app imports
import Display ( Direction(Up, Down, Left, Right), bumpCursor, newFullscreenWindow )
import Editor ( backspace )
import Comms.HipChat (startClient)
import Geometry

-- Windows
import ChatRoom ( createAndManageChatRoomWindow )
import Help ( createAndManageHelpWindow )
import Rooms ( createAndManageRoomsWindow )
import People ( createAndManagePeopleWindow )

-- NCurses imports
import UI.NCurses

-- XMPP imports
{-import Network-}
{-import Network.Protocol.XMPP-}
{-import Data.XML.Types-}

-- Other imports
import Data.Char
import Data.Either
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import qualified Data.Text as T
import System.Environment

type BetterWindow = (Window, (Integer, Integer, Integer, Integer))

main :: IO ()
main = do
    startInterface
    {-startComms-}
    {-threadId <- forkIO task1-}
    {-task2-}

task1 :: IO ()
task1 = forever $ do
    putStrLn "activity ..."
    threadDelay 1000000

task2 :: IO ()
task2 = forever $ do
    putStrLn "slow ..."
    threadDelay 2000000

startComms :: IO ()
startComms = do
    putStrLn "Starting communications"
    user <- getEnv "HIPCHAT_USER"
    pass <- getEnv "HIPCHAT_PASSWORD"
    let server = "chat.hipchat.com"
    startClient server (T.pack user) (T.pack pass)
    startInterface
    startComms

startInterface :: IO ()
startInterface = runCurses $ do
    setEcho False
    w <- defaultWindow
    (rows, columns) <- screenSize
    editorWindow <- newWindow 10 columns (rows - 12) 0
    let editorWindowWithCoords = (editorWindow, (10, columns, (rows - 12), 0))
    editorColorId <- newColorID ColorBlue ColorBlack 1
    powerlineColourId <- newColorID ColorWhite ColorBlue 2
    titleColourId <- newColorID ColorWhite ColorBlack 3
    updateWindow w $ do
        let title = "HipChat - Lobby"
        moveCursor 1 $ calcOffset columns title
        setColor titleColourId
        drawString title
        setColor defaultColorID
        moveCursor 3 1
        drawString "Open Chat Rooms: 27"
        moveCursor 0 0
        drawLineH Nothing columns
        moveCursor 2 0
        drawLineH Nothing columns
        moveCursor (rows - 2) 0
        setColor powerlineColourId
        let powerlineText = "Status: Avaliable"
        drawString $ " " ++ powerlineText ++ (take ((fromIntegral columns) - 1 - length powerlineText) $ repeat ' ')
    {-updateWindow editorWindow $ do-}
        {-setColor editorColorId-}
        {-drawBorder Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing-}
        {-moveCursor 1 1-}
    render
    {-waitFor w eventHandler-}
    waitFor editorWindow eventHandler

{-type Editor = Either Bool String-}
type Buffer = State Window String

waitFor :: Window -> (Window -> Event -> Curses (Bool)) -> Curses ()
waitFor w p = loop where
    loop = do
        ev <- getEvent w Nothing
        case ev of
            Nothing  -> loop
            Just ev' -> (p w ev') >>= (\b -> if not b then return () else loop)

eventHandler :: Window -> Event -> Curses (Bool)
eventHandler window (EventSpecialKey key) = eventSpecialKeyHandler window key
eventHandler window (EventCharacter char) = eventCharacterHandler window char
eventHandler _ EventResized               = return True
eventHandler _ _                          = return True

eventSpecialKeyHandler :: Window -> Key -> Curses (Bool)
eventSpecialKeyHandler window key
    | key == KeyDownArrow  = bumpCursor window Down >>= (\_ -> return True)
    | key == KeyUpArrow    = bumpCursor window Up >>= (\_ -> return True)
    | key == KeyLeftArrow  = bumpCursor window Display.Left >>= (\_ -> return True)
    | key == KeyRightArrow = fmap (\_ -> True) $ bumpCursor window Display.Right
    | key == KeyBackspace  = fmap (\_ -> True) $ bumpCursor window Display.Left
    | otherwise            = return True

eventCharacterHandler :: Window -> Char -> Curses (Bool)
eventCharacterHandler window char
    | char == 'q'  || char == 'Q' = return False
    | char == chr 127             = backspace window >>= (\_ -> return True)
    | char == 'c'                 = createAndManageChatRoomWindow >>= (\_ -> render) >>= (\_ -> return True)
    | char == 'h'                 = createAndManageHelpWindow >>= (\_ -> render) >>= (\_ -> return True)
    | char == 'r'                 = createAndManageRoomsWindow >>= (\_ -> render) >>= (\_ -> return True)
    | char == 'p'                 = createAndManagePeopleWindow >>= (\_ -> render) >>= (\_ -> return True)
    | otherwise                   = return True
    {-| otherwise                   = do-}
                                        {-updateWindow window $ drawString [char]-}
                                        {-render-}
                                        {-return True-}
