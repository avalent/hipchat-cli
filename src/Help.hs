module Help ( createAndManageHelpWindow ) where

-- HipChat app imports
import Display ( Direction(Up, Down, Left, Right), bumpCursor, newFullscreenWindow, drawPowerline, drawInfoText  )
import Editor ( backspace )
import Comms.HipChat (startClient)
import Geometry

-- NCurses imports
import UI.NCurses

-- XMPP imports
{-import Network-}
{-import Network.Protocol.XMPP-}
{-import Data.XML.Types-}

-- Other imports
import Data.Char
import Data.Either
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import qualified Data.Text as T
import System.Environment

createAndManageHelpWindow :: Curses ()
createAndManageHelpWindow = do
    setEcho False
    helpWindow <- newFullscreenWindow
    {-setCursorMode CursorInvisible-}
    (rows, columns) <- screenSize
    highlightedRowColour <- newColorID ColorWhite ColorGreen 1
    updateWindow helpWindow $ do
        let title = "HipChat - Help"
        moveCursor 1 $ calcOffset columns title
        drawString title
        moveCursor 0 0
        drawLineH Nothing columns
        moveCursor 2 0
        drawLineH Nothing columns
    drawPowerline helpWindow "Status: Available"
    drawInfoText helpWindow "Cannot move beyone the last line"
    drawScreen helpWindow 3
    render
    waitFor helpWindow eventHandler
    closeWindow helpWindow
    render

drawScreen :: Window -> Integer -> Curses ()
drawScreen window currentRow = paint window 3 currentRow [
    "(help): press q to quit", 
    "(help): press c to create a new room",
    "(help): press h to show help",
    "(help): press r to show rooms",
    "(help): press p to show people"
    ]

paint :: Window -> Integer -> Integer -> [String] -> Curses ()
paint window _ hrow []       = updateWindow window $ moveCursor hrow 0
{-paint window row hrow []       = paint' window hrow hrow " "-}
paint window row hrow (x:xs) = paint' window row hrow x >>= (\_ -> paint window (row + 1) hrow xs)

paint' :: Window -> Integer -> Integer -> String -> Curses ()
paint' window row hrow line = do
    (_, columns) <- screenSize
    highlightedRowColour <- newColorID ColorWhite ColorGreen 1
    let c = if (row == hrow) then highlightedRowColour else defaultColorID
    updateWindow window $ do
        moveCursor (toInteger row) 0
        setColor c
        drawString " "
        drawString line
        drawString $ take ((fromIntegral columns) - 1 - length line) $ repeat ' '
        setColor defaultColorID

waitFor :: Window -> (Window -> Event -> Curses (Bool)) -> Curses ()
waitFor w p = loop where
    loop = do
        ev <- getEvent w Nothing
        case ev of
            Nothing  -> loop
            Just ev' -> (p w ev') >>= (\b -> if not b then return () else loop)

eventHandler :: Window -> Event -> Curses (Bool)
eventHandler window (EventSpecialKey key) = eventSpecialKeyHandler window key
eventHandler window (EventCharacter char) = eventCharacterHandler window char
eventHandler _ EventResized               = return True
eventHandler _ _                          = return True

eventSpecialKeyHandler :: Window -> Key -> Curses (Bool)
eventSpecialKeyHandler window key
    | key == KeyDownArrow  = bumpCursor window Down >>= (\_ -> return True)
    | key == KeyUpArrow    = bumpCursor window Up >>= (\_ -> return True)
    | key == KeyLeftArrow  = bumpCursor window Display.Left >>= (\_ -> return True)
    | key == KeyRightArrow = fmap (\_ -> True) $ bumpCursor window Display.Right
    | key == KeyBackspace  = fmap (\_ -> True) $ bumpCursor window Display.Left
    | otherwise            = return True

eventCharacterHandler :: Window -> Char -> Curses (Bool)
eventCharacterHandler window char
    | char == 'q'  || char == 'Q' = return False
    {-| char == 'j'                 = bumpCursor window Down >>= (\_ -> return True)-}
    | char == 'j'                 = do
        (row, _) <- getCursor window
        drawScreen window (row+1)
        render
        return True
    | char == 'k'                 = getCursor window >>= (\(x,_) -> drawScreen window (x-1)) >>= (\_ -> render) >>= (\_ -> return True)
    {-| char == 'k'                 = bumpCursor window Up >>= (\_ -> return True)-}
    | otherwise                   = return True
