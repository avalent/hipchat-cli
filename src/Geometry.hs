module Geometry ( calcOffset ) where

calcOffset :: Integer -> String -> Integer
calcOffset screenWidth string = toInteger $ (screenWidth `div` 2) - (fromIntegral (length string) `div` 2)

