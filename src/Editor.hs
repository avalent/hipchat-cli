module Editor ( backspace ) where

import UI.NCurses
import Display ( Direction(Up, Down, Left, Right), bumpCursor )

-- Need to make this more advanced:
--   1. Delete character under curser
--   2. Move curser left
--   3. Move it up if at the end of the line
backspace :: Window -> Curses ()
backspace window = do
    bumpCursor window Display.Left >>= (\_ -> return ())
    updateWindow window $ drawString " "
    bumpCursor window Display.Left >>= (\_ -> return ())
