module Display ( Direction (Up, Down, Left, Right), newFullscreenWindow, bumpCursor, drawPowerline, drawInfoText  ) where

import UI.NCurses
import UI.NCurses.Panel
import Control.Monad
import Control.Monad.IO.Class

-- Create a new fullscreen window
newFullscreenWindow :: Curses Window
newFullscreenWindow = newWindow 0 0 0 0

data Direction = Up | Down | Left | Right deriving (Eq, Show)

-- We can optionally return a message when the user tried to
-- move off the screen.
--
-- FIXME: This only works for fullscreen windows at the moment
bumpCursor :: Window -> Direction -> Curses (Maybe String)
bumpCursor window direction = do
    (row, column) <- getCursor window
    {-stdscr <- defaultWindow-}
    {-(row, column) <- getCursor stdscr-}
    let (row', column') = (newRow row, newColumn column)
    isEnclosed <- enclosed window row' column'
    let (x, y) = if isEnclosed then (row', column') else (row, column)
    {-(rows, columns) <- screenSize-}
    {-liftIO $ putStrLn $ "isEnclosed: " ++ show isEnclosed-}
    {-liftIO $ putStrLn $ "rows: " ++ show rows ++ " row: " ++ show row-}
    {-liftIO $ putStrLn $ "row: " ++ show row-}
    {-liftIO $ putStrLn $ "cols: " ++ show columns-}
    updateWindow window $ do
        moveCursor x y
    render
    return Nothing
    where
        newRow row
            | direction == Up   = row - 1
            | direction == Down = row + 1
            | otherwise         = row
        newColumn column
            | direction == Display.Left  = column - 1
            | direction == Display.Right = column + 1
            | otherwise = column

powerlineColourId :: Curses (ColorID)
powerlineColourId = newColorID ColorWhite ColorBlue 2

greenTextColourId :: Curses (ColorID)
greenTextColourId = newColorID ColorGreen ColorBlack 3

-- Since not sure how to get the window dimensions this assumes
-- the window is fullscreen
drawPowerline :: Window -> String -> Curses ()
drawPowerline window text = do
    plColour <- powerlineColourId
    (rows, columns) <- screenSize
    updateWindow window $ do
        moveCursor (rows - 2) 0
        setColor plColour
        drawString $ text ++ (take ((fromIntegral columns) - length text) $ repeat ' ')
        setColor defaultColorID

-- Assumes the window is fullscreen
drawInfoText :: Window -> String -> Curses ()
drawInfoText window text = do
    textColour <- greenTextColourId
    (rows, columns) <- screenSize
    updateWindow window $ do
        moveCursor (rows - 1) 0
        setColor textColour
        drawString $ text ++ (take ((fromIntegral columns) - 1 - length text) $ repeat ' ')
        setColor defaultColorID

-- Perhaps move this into a TerminalEvents module
{-waitFor :: Window -> (Event -> Bool) -> Curses ()-}
{-waitFor w p = loop where-}
    {-loop = do-}
        {-ev <- getEvent w Nothing-}
        {-case ev of-}
            {-Nothing -> loop-}
            {-Just ev' -> if p ev' then return () else loop-}
