module Rooms ( createAndManageRoomsWindow ) where

-- HipChat app imports
import Display ( Direction(Up, Down, Left, Right), bumpCursor, newFullscreenWindow )
import Editor ( backspace )
import Comms.HipChat (startClient)
import Geometry

-- NCurses imports
import UI.NCurses
import UI.NCurses.Panel

-- XMPP imports
{-import Network-}
{-import Network.Protocol.XMPP-}
{-import Data.XML.Types-}

-- Other imports
import Data.Char
import Data.Either
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import qualified Data.Text as T
import System.Environment

createAndManageRoomsWindow :: Curses ()
createAndManageRoomsWindow = do
    setEcho False
    {-setEcho True-}
    setCursorMode CursorInvisible
    roomWindow <- newFullscreenWindow
    (rows, columns) <- screenSize
    updateWindow roomWindow $ do
        let title = "HipChat - Rooms"
        moveCursor 1 $ calcOffset columns title
        drawString title
        moveCursor 3 1
        drawString "(room): PoolMan"
        moveCursor 4 1
        drawString "(room): Hoverboard"
        moveCursor 5 1
        drawString "(room): Armata"
        moveCursor 0 0
        drawLineH Nothing columns
        moveCursor 2 0
        drawLineH Nothing columns
    {-infoPanelWindow <- newWindow 10 10 10 10-}
    infoPanel <- newPanel roomWindow
    {-replacePanelWindow infoPanel infoPanelWindow-}
    {-infoPanelWindow <- getPanelWindow infoPanel-}
    {-updateWindow infoPanelWindow $ do-}
        {-moveCursor 10 10-}
        {-drawString "Ashley"-}
    {-movePanel infoPanel 20 20-}
    drawScreen roomWindow 3
    render
    waitFor roomWindow eventHandler
    {-waitFor infoPanelWindow eventHandler-}
    closeWindow roomWindow
    render

drawScreen :: Window -> Integer -> Curses ()
drawScreen window currentRow = paint window 3 currentRow [
    "(room): Armata",
    "(room): Dev Tools",
    "(room): Engineering Services",
    "(room): Football",
    "(room): Gradlassian",
    "(room): HipChat",
    "(room): Hoverboard",
    "(room): PoolMan", 
    "(room): Serval"
    ]

paint :: Window -> Integer -> Integer -> [String] -> Curses ()
paint window _ hrow []       = updateWindow window $ moveCursor hrow 0
{-paint window row hrow []       = paint' window hrow hrow " "-}
paint window row hrow (x:xs) = paint' window row hrow x >>= (\_ -> paint window (row + 1) hrow xs)

paint' :: Window -> Integer -> Integer -> String -> Curses ()
paint' window row hrow line = do
    (_, columns) <- screenSize
    highlightedRowColour <- newColorID ColorWhite ColorGreen 1
    let c = if (row == hrow) then highlightedRowColour else defaultColorID
    updateWindow window $ do
        moveCursor (toInteger row) 0
        setColor c
        drawString " "
        drawString line
        drawString $ take ((fromIntegral columns) - 1 - length line) $ repeat ' '
        setColor defaultColorID



waitFor :: Window -> (Window -> Event -> Curses (Bool)) -> Curses ()
waitFor w p = loop where
    loop = do
        ev <- getEvent w Nothing
        case ev of
            Nothing  -> loop
            Just ev' -> (p w ev') >>= (\b -> if not b then return () else loop)

eventHandler :: Window -> Event -> Curses (Bool)
eventHandler window (EventSpecialKey key) = eventSpecialKeyHandler window key
eventHandler window (EventCharacter char) = eventCharacterHandler window char
eventHandler _ EventResized               = return True
eventHandler _ _                          = return True

eventSpecialKeyHandler :: Window -> Key -> Curses (Bool)
eventSpecialKeyHandler window key
    | key == KeyDownArrow  = bumpCursor window Down >>= (\_ -> return True)
    | key == KeyUpArrow    = bumpCursor window Up >>= (\_ -> return True)
    | key == KeyLeftArrow  = bumpCursor window Display.Left >>= (\_ -> return True)
    | key == KeyRightArrow = fmap (\_ -> True) $ bumpCursor window Display.Right
    | key == KeyBackspace  = fmap (\_ -> True) $ bumpCursor window Display.Left
    | otherwise            = return True

eventCharacterHandler :: Window -> Char -> Curses (Bool)
eventCharacterHandler window char
    | char == 'q'  || char == 'Q' = return False
    | char == 'j'                 = bumpCursor window Down >>= (\_ -> return True)
    | char == 'k'                 = bumpCursor window Up >>= (\_ -> return True)
    | otherwise                   = return True
