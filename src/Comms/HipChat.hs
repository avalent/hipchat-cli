{-# LANGUAGE OverloadedStrings #-}
module Comms.HipChat (startClient) where

-- XMPP imports
import Network
import Network.Protocol.XMPP
import Data.XML.Types

-- Other imports
import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Text as T

startClient :: String -> T.Text -> T.Text -> IO ()
startClient hostname user password = do
    -- Verify that the user provided a valid Jabber ID (JID)
    jid <- case parseJID user of
        Just x  -> return x
        Nothing -> error $ "Invalid JID: " ++ show user
    username <- case strNode `fmap` jidNode jid of
        Just x  -> return x
        Nothing -> error "JID must include a username"

    -- 'Server' values record what host the connection will be opened to. Normally
    -- the hostname and JID will be the same; however, in some cases the hostname is
    -- something special (like "jabber.domain.com" or "localhost").
    --
    -- The port number is hardcoded to 5222 in this example, but in the wild there
    -- might be servers with a jabberd running on alternative ports.
    let server = Server {
        serverHostname = hostname
        , serverJID = JID Nothing (jidDomain jid) Nothing
        , serverPort = PortNumber 5222
    }

    res <- runClient server jid username password $ do
        boundJID <- bindJID jid

        -- Some servers will close the XMPP connection after some period
        -- of inactivity. For this example, we'll simply send a "ping" every
        -- 60 seconds
        _ <- getSession >>= liftIO . forkIO . sendPings 60

        liftIO $ putStrLn $ "Server bound our session to: " ++ show boundJID

        -- This is a simple loop which will echo received messages back to the
        -- sender; additionally, it prints *all* received stanzas to the console.
        forever $ do
            stanza <- getStanza
            liftIO $ putStr "\n" >> print stanza >> putStrLn "\n"
            case stanza of
                ReceivedMessage msg  -> unless (messageType msg == MessageError) $ putStanza $ echo msg
                ReceivedPresence msg -> when (presenceType msg == PresenceSubscribe) $ putStanza (subscribe msg)
                _                    -> return ()

    -- If 'runClient' terminated due to an XMPP error, propagate it as an exception.
    -- In non-example code, you might want to show this error to the user.
    case res of
        Left err -> error $ show err
        Right _  -> return ()

-- Copy a 'Message' into another message, setting the 'messageTo' field to the
-- original sender's address.
echo :: Message -> Message
echo msg = Message
    { messageType = MessageNormal
    , messageTo = messageFrom msg

    -- Note: Conforming XMPP servers populate the "from" attribute on
    -- stanzas, to prevent clients from spoofing it. Therefore, the
    -- 'messageFrom' field's value is irrelevant when sending stanzas.
    , messageFrom = Nothing
    , messageID = Nothing
    , messageLang = Nothing
    , messagePayloads = messagePayloads msg
    }

subscribe :: Presence -> Presence
subscribe p = Presence
    { presenceType = PresenceSubscribed
    , presenceTo = presenceFrom p
    , presenceFrom = Nothing
    , presenceID = Nothing
    , presenceLang = Nothing
    , presencePayloads = []
    }

-- Send a "ping" occasionally, to prevent server timeouts from
-- closing the connection.
-- Connections are dropped after 150s of inactivity. We suggest
-- sending a single space (" ") as keepalive data every 60 seconds.
sendPings :: Integer -> Session -> IO ()
sendPings seconds s = forever send where
    send = do
        -- Ignore errors
        _ <- runXMPP s $ putStanza ping
        threadDelay $ fromInteger $ 1000000 * seconds
    ping = (emptyIQ IQGet)
        { iqPayload = Just (Element pingName [] [])
        }

pingName :: Name
pingName = Name "ping" (Just "urn:xmpp:ping") Nothing

getRooms :: Message
getRooms = Message
    {
      messageFrom = Nothing
    , messageID = Nothing 
    }
