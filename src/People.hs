module People ( createAndManagePeopleWindow ) where

-- HipChat app imports
import Display ( Direction(Up, Down, Left, Right), bumpCursor, newFullscreenWindow )
import Editor ( backspace )
import Comms.HipChat (startClient)
import Geometry

-- NCurses imports
import UI.NCurses

-- XMPP imports
{-import Network-}
{-import Network.Protocol.XMPP-}
{-import Data.XML.Types-}

-- Other imports
import Data.Char
import Data.Either
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import qualified Data.Text as T
import System.Environment

createAndManagePeopleWindow :: Curses ()
createAndManagePeopleWindow = do
    setEcho False
    helpWindow <- newFullscreenWindow
    (rows, columns) <- screenSize
    updateWindow helpWindow $ do
        let title = "HipChat - People"
        moveCursor 1 $ calcOffset columns title
        drawString title
        moveCursor 0 0
        drawLineH Nothing columns
        moveCursor 2 0
        drawLineH Nothing columns
    render
    waitFor helpWindow eventHandler
    closeWindow helpWindow
    render

waitFor :: Window -> (Window -> Event -> Curses (Bool)) -> Curses ()
waitFor w p = loop where
    loop = do
        ev <- getEvent w Nothing
        case ev of
            Nothing  -> loop
            Just ev' -> (p w ev') >>= (\b -> if not b then return () else loop)

eventHandler :: Window -> Event -> Curses (Bool)
eventHandler window (EventSpecialKey key) = eventSpecialKeyHandler window key
eventHandler window (EventCharacter char) = eventCharacterHandler window char
eventHandler _ EventResized               = return True
eventHandler _ _                          = return True

eventSpecialKeyHandler :: Window -> Key -> Curses (Bool)
eventSpecialKeyHandler window key
    | key == KeyDownArrow  = bumpCursor window Down >>= (\_ -> return True)
    | key == KeyUpArrow    = bumpCursor window Up >>= (\_ -> return True)
    | key == KeyLeftArrow  = bumpCursor window Display.Left >>= (\_ -> return True)
    | key == KeyRightArrow = fmap (\_ -> True) $ bumpCursor window Display.Right
    | key == KeyBackspace  = fmap (\_ -> True) $ bumpCursor window Display.Left
    | otherwise            = return True

eventCharacterHandler :: Window -> Char -> Curses (Bool)
eventCharacterHandler window char
    | char == 'q'  || char == 'Q' = return False
    | char == 'j'                 = bumpCursor window Down >>= (\_ -> return True)
    | char == 'k'                 = bumpCursor window Up >>= (\_ -> return True)
    | otherwise                   = return True
